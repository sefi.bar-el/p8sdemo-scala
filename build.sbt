name := "prom-demo"

version := "0.1"

scalaVersion := "2.13.6"

libraryDependencies += "io.kamon" %% "kamon-prometheus" % "2.5.4"
