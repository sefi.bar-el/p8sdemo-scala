import kamon.Kamon


object PromDemo extends App {
  Kamon.init()
  val myCounter = Kamon.counter("my.counter", "my awesome counter!!!").withoutTags()

  while (true) {
    myCounter.increment()
    Thread.sleep(1000)
  }

  Kamon.stopModules()
}
